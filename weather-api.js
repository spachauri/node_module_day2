import rl from 'readline';
import axios from 'axios';

const readline = rl.createInterface({
  input: process.stdin,
  output: process.stdout,
});
let path = 'https://api.openweathermap.org/data/2.5/weather?q=India&appid=c3c34a5ac697fd4fae23492e536814da';
let query = 'India';// default query

readline.question(
  'Enter name of the place(City/State/Country) to see temperature ?',
  (name) => {
    query = name;
    console.log(`query is ${query}`);
    path = `https://api.openweathermap.org/data/2.5/weather?q=${query}&appid=c3c34a5ac697fd4fae23492e536814da`;
    console.log(`path is ${path}`);
    readline.close();
  },
);
const callApi = () => {
  axios
    .get(path)
    .then((response) => {
      console.log(`Maximum temperature of ${query}: `);
      console.log(`${response.data.main.temp_max} Kelvin`);
      console.log(`Maximum temperature of ${query}: `);
      console.log(`${response.data.main.temp_min} Kelvin`);
      console.log(response.data.main);
    })
    .catch((error) => {
      console.log(error);
    });
};
setTimeout(callApi, 8000);
console.log('Welcome to weather api');
